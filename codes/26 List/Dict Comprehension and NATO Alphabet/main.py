lst = [data for data in range(2,20,2)]
print(lst)
lst = [data+1 for data in range(2,20,2)]
print(lst)
data = "This is my String"
lst = [letter for letter in data]
print(lst)
print([ i for i in range(3,31,3)])
print([ i*2 for i in range(3,31,3)])
print([ i*2 for i in range(3,31,3) if i%2 == 0])



sentence = "What is the Airspeed Velocity of an Unladen Swallow?"
# Don't change code above 👆

# Write your code below:
sentence = sentence.split()
result = {i:len(i) for i in sentence}
print(result)

new_result = {key:value for (key,value) in result.items() if value > 2}
print(new_result)