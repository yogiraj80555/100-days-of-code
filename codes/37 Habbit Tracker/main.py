from datetime import datetime
import requests

PIXELA_ENDPOINT = "https://pixe.la/v1"
PIXELA_User_creation_ENDPOINT = PIXELA_ENDPOINT+"/users"
PIXELA_GRAPH_CREATION_ENDPOINT = PIXELA_ENDPOINT+"/users/{}/graphs"
PIXELA_GRAPH_VALUE = "/users/{}/graphs/{}"
PIXELA_PUT_GRAPH_VALUE = PIXELA_ENDPOINT + PIXELA_GRAPH_VALUE
USERNAME = "python-learn"
GRAPH_ID = "python1graph"
SERCET = "gaue626alkdye5kafhwte6"


def hit_post(url,params,jsonData,header=None):
    response = requests.post(url,params,json=jsonData,headers=header);
    if response.status_code == 200:
        return response.json()
    return response.text;

def createUser():
    user_params = {
        "token" : "gaue626alkdye5kafhwte6",
        "username" : "python-learn",
        "agreeTermsOfService" : "yes",
        "notMinor" : "yes"
    }
    response = hit_post(PIXELA_User_creation_ENDPOINT,"",user_params);
    print(response)

def graph_creation(username,secret):
    graph_params = {
        "id":"python1graph",
        "name":"Python Learn Graph",
        "unit":"kilometer",
        "type":"float",
        "color":"shibafu"
    }

    graph_header = {
        "X-USER-TOKEN" : secret
    }
    url = PIXELA_GRAPH_CREATION_ENDPOINT.format(username);
    response = hit_post(url,"",graph_params,header=graph_header)
    print(response)

def postValuetoGraph(secret):
    url = PIXELA_PUT_GRAPH_VALUE.format(USERNAME,GRAPH_ID)
    val = 20221030
    date  = datetime.now().strftime("%Y%m%d")
    for i in range(28):
        graph_params = {
            "date": str(date),
            "quantity": "6",
        }

        graph_header = {
            "X-USER-TOKEN" : secret
        }
        
        response = hit_post(url,"",graph_params,graph_header)
        print(response)
        val = val + 0
        break;


#createUser()
#graph_creation("python-learn","gaue626alkdye5kafhwte6")
postValuetoGraph(SERCET)
#print(datetime.now().year,datetime.now().month,datetime.now().date(),datetime.now().strftime("%Y%m%d"))