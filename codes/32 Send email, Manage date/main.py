##################### Extra Hard Starting Project ######################

import smtplib
import datetime as dt
import random
import pandas

def open_csv():
    data = pandas.read_csv("birthday Wisher/birthdays.csv")
    now = dt.datetime.now()
    month = now.month
    day = now.day
    row = data[data.month == month]
    row = row[row.day == day]
    return row
    
def open_letter(name):
    with open(f"birthday Wisher/letter_templates/letter_{random.randint(1,3)}.txt") as f:
        data = f.read()
        data = data.replace("[NAME]", name)
        data = data.replace("Angela", "Yogiraj")
    return data

def send_mail(to_address, message):
    my_email="yogiraj80555@outlook.com"
    password=""
    with smtplib.SMTP(host="smtp-mail.outlook.com", port=587) as connection:
        connection.starttls()
        connection.login(user=my_email,password=password)
        connection.sendmail(from_addr=my_email, 
                            to_addrs=to_address,
                            msg=f"Subject:Happy Birthday!!!\n\n{message}")


row = open_csv()
print(f"Data: {row.name.iloc[0]}")

if not row.empty:
    data = open_letter(row["name"].iloc[0])
    send_mail(row["email"].iloc[0], data)


