from turtle import Turtle,Screen
import random

screen = Screen()
screen.setup(width=500, height=400);
ch = screen.textinput(title="Turtle Color",prompt="Select yourt turtle color")

colors = ["red","orange","yellow","green","blue","purple"];
y_positions = [-80, -50, -20, 10, 40, 70]
turtles = []
for turtle_index in range(6):
    tim = Turtle(shape="turtle")
    tim.penup()
    tim.color(colors[turtle_index])
    tim.goto(x=-240,y=y_positions[turtle_index])
    turtles.append(tim)

loop = True
while loop:
    for tur in turtles:
        if tur.xcor() > 230:
            loop = False
            color = tur.pencolor()
            if ch == color:
                print("You Won")
            else:
                print(f"{color} color is won the race.")
            break;
        distance = random.randint(0,10)
        tur.forward(distance)
        

screen.exitonclick()

