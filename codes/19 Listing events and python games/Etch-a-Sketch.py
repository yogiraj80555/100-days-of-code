from turtle import Turtle,Screen
tim = Turtle()
screen = Screen()


def move_forwards():
    tim.forward(10)


def move_left():
    tim.left(10)
    # OR this can be
    # tim.setheading(tim.heading() + 10)

def move_right():
    tim.right(10)
    # OR this can be
    # tim.setheading(tim.heading() - 10)

def move_back():
    tim.backward(10)

def clear_screen():
    tim.clear(); # get rid of all drawing that turtle did
    tim.penup()
    tim.home(); # bring cursor at initial starting position
    tim.pendown()

screen.listen()
screen.onkey(key='w', fun=move_forwards) #the function passing into function is know as higher order function here onKey() is higher order function
screen.onkey(key='s', fun=move_back)
screen.onkey(key='a', fun=move_left)
screen.onkey(key='d', fun=move_right)
screen.onkey(key='c', fun=clear_screen)
screen.exitonclick()


