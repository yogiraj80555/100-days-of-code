from turtle import Turtle,Screen

tim = Turtle()
screen = Screen()


def move_forwards():
    tim.forward(10)


screen.listen()
screen.onkey(key='space', fun=move_forwards) #the function passing into function is know as higher order function here onKey() is higher order function
screen.exitonclick()