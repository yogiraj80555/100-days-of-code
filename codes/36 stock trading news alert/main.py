import requests


STOCK_NAME="TSLA"
stock_key = "NEBJY8SJYJUWGTDT"
news_key = "70fe2c8d15cd4d22945e24182761260d"
alphavantage = "https://www.alphavantage.co/query"
news = "https://newsapi.org/v2/everything" 

def getresponse(url,params):
    response = requests.get(url,params)
    if response.status_code == 200:
        return response.json()
    return None;


#getting Stock
def gettingStocks():
    params={
        "function" : "TIME_SERIES_DAILY",
        "symbol" : STOCK_NAME,
        "apikey" : stock_key,
        "outputsize" : "compact"
    }
    response = getresponse(alphavantage,params)
    daily_data = response["Time Series (Daily)"]
    counter = 0
    closing = []
    keys = []
    for key,value in daily_data.items():
        closing.append(value["4. close"])
        keys.append(key)
        counter += 1
        if(counter == 2):
            break
    print(closing)
    diff = float(closing[0]) - float(closing[1]) #if difference is +ve is up if -ve is down
    diff = abs(diff)
    val = (diff / float(closing[0]))*100
    
    print(val)
    if val > 0.5 :
        top_title = getNews(keys)
        print(top_title)
    #     send_SMS(top_title) #for this see 35 day code
    
def getNews(keys):
    params = {
        "q" : "tesla",
        "from" : keys[0],
        "sortBy" : "publishedAt",
        "apiKey": news_key,
    }
    response = getresponse(news,params)
    titles = []
    count = 0
    for data in response["articles"]:
        titles.append(data["title"])
        count += 1
        if count == 3:
            break;
    return titles


gettingStocks()
