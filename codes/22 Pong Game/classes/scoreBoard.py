from turtle import Turtle


TEXT_ALIGN = "center"
FONT = ("courier",10,"italic bold")

class ScoreBoard(Turtle):

    def __init__(self,side,x_pos,y_pos):
        super().__init__()
        self.score = 0
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(x=x_pos, y=y_pos)
        self.writeScore(side)

    def writeScore(self,side):
        self.write(self.getScroreBoardText(side),
            align=TEXT_ALIGN,
            font=FONT)

    def getScroreBoardText(self,side):
        return f"{side} Score: "+str(self.score);

    def incrementScore(self,side):
        self.clear()
        self.score += 1
        self.writeScore(side)