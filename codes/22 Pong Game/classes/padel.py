from turtle import Turtle

MOVE_DISTANCE = 20
#directions
UP = 90
DOWN = 270

class Padel(Turtle):

    def __init__(self,width,height,x_pos,y_pos):
        super().__init__()
        self.shape("square")
        strch_wid = height // width
        #default is 20 * 20 we want width is 100 which is 5 times of 20
        self.shapesize(stretch_wid=strch_wid, stretch_len=1)
        self.goto(x_pos, y_pos)
        self.color("white")
        self.penup()

    def moveUp(self):
        x_pos = self.xcor()
        y_pos = self.ycor()
        if y_pos > 220:
            return
        self.goto(x_pos, y_pos+MOVE_DISTANCE)

    def moveDown(self):
        x_pos = self.xcor()
        y_pos = self.ycor()
        if y_pos < -220:
            return
        self.goto(x_pos, y_pos-MOVE_DISTANCE)

    