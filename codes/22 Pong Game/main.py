import time
from turtle import Screen
from classes.padel import Padel
from classes.ball import Ball
from classes.scoreBoard import ScoreBoard


screen = Screen()
screen.bgcolor("black")
screen.setup(width=800, height=600)
screen.title("The Pong Game")
screen.tracer(0)

r_padel = Padel(width=20,height=100,x_pos=380,y_pos=0)
l_padel = Padel(width=20,height=100,x_pos=-385,y_pos=0)
ball = Ball();
r_score = ScoreBoard("Right",200,280);
l_score = ScoreBoard("Left",-200,280);


screen.listen()
screen.onkey(r_padel.moveUp, "Up")
screen.onkey(r_padel.moveDown, "Down")
screen.onkeypress(l_padel.moveUp, "w")
screen.onkeypress(l_padel.moveDown, "s")


while True:
    time.sleep(ball.move_speed)
    screen.update()
    ball.move()

    #detect Collision to bounce
    if ball.ycor() > 280 or ball.ycor() < -290:
        #need to bounce
        ball.bounce()

    if (ball.distance(r_padel) < 50 and ball.xcor() > 350) or (ball.distance(l_padel) < 50 and ball.xcor() < -350) :
        ball.reverse_x()

    # Detect R paddel misses a ball
    if ball.xcor() > 390:
        ball.reset()
        l_score.incrementScore("Left")

    
    # Detect L paddel misses a ball
    if ball.xcor() < -390:
        ball.reset()
        r_score.incrementScore("Right")
        

screen.exitonclick()

