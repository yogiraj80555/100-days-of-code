import random
from turtle import Turtle

COLORS = ["navy", "dim gray", "slate gray", "light sky blue",
    "dark cyan", "medium sea green", "dark khaki", "yellow",
    "dark goldenrod", "burlywood", "orange", "maroon", "peach puff",
    "indian red", "deep pink", "purple", "dark slate blue"]

MOVING_SPEED = 10

class Capstone(Turtle):

    def __init__(self):
        super().__init__();
        self.shape("square")
        self.penup()
        self.shapesize(stretch_wid=0.8, stretch_len=1.5)
        self.color(random.choice(COLORS))
        self.goto(x=290,y=random.randint(-240,280))
        self.setheading(180)

    def movingForward(self,player,speed):
        self.forward(speed)
        if self.distance(player) < 20 :
            return True
        return False;

        
       