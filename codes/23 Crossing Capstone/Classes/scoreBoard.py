from turtle import Turtle


TEXT_ALIGN = "center"
FONT = ("courier",10,"italic bold")

class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.color("black")
        self.penup()
        self.hideturtle()
        self.goto(x=-250, y=280)
        self.writeScore()

    def writeScore(self):
        self.write(self.getScroreBoardText(),
            align=TEXT_ALIGN,
            font=FONT)

    def getScroreBoardText(self):
        return f"Score: "+str(self.score);


    def incrementScore(self):
        self.clear()
        self.score += 1
        self.writeScore()