from turtle import Turtle

UP = 90
MOVING_SPEED = 10

class TurtleObject(Turtle):

    def __init__(self):
        super().__init__();
        self.shape("turtle")
        self.penup()
        self.setheading(UP)
        self.goto(x=0,y=-280)
        
    def moveForward(self):
        #This must be win condition
        if self.ycor() > 290:
            return
        self.forward(MOVING_SPEED)

    def moveBackward(self):
        if self.ycor() < -280:
            return
        self.backward(MOVING_SPEED)

    def moveRight(self):
        x_pos = self.xcor() + MOVING_SPEED
        if x_pos > 290:
            return
        self.goto(x = x_pos, y = self.ycor())

    def moveLeft(self):
        x_pos = self.xcor() - MOVING_SPEED
        if x_pos < -290:
            return
        self.goto(x = x_pos, y = self.ycor())

    def is_at_Finish(self):
        if self.ycor() > 280:
            return True
        return False

    def gotoStart(self):
        self.goto(x=0,y=-280)

    


    