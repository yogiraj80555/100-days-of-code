from turtle import Turtle


TEXT_ALIGN = "center"
FONT = ("courier",14,"italic bold")


class GameOver(Turtle):

    def __init__(self,score):
        super().__init__()
        self.score = 0
        self.color("black")
        self.penup()
        self.hideturtle()
        self.goto(x=0, y=0)
        self.writeScore(score)

    def writeScore(self,score):
        self.write(self.getScroreBoardText(score),
            align=TEXT_ALIGN,
            font=FONT)

    def getScroreBoardText(self,score):
        return f"Game Over!\nScore: "+str(score);