import time
from turtle import Screen,Turtle
from Classes.turtleObject import TurtleObject
from Classes.movingCapstone import Capstone
from Classes.scoreBoard import ScoreBoard
from Classes.gameover import GameOver

screen = Screen()
screen.setup(width=600, height=600)
screen.title("The Crossing Capstone")
screen.tracer(0)

turtle = TurtleObject()
score = ScoreBoard()

screen.listen()
screen.onkeypress(turtle.moveForward, "Up")
screen.onkeypress(turtle.moveBackward, "Down")
screen.onkeypress(turtle.moveRight, "Right")
screen.onkeypress(turtle.moveLeft, "Left")


cars = []
cap = 0
game_on = True
speed = 10
while game_on:
    time.sleep(0.05)
    screen.update()
    if cap%10 == 0:
        cap = 0
        cars.append(Capstone())
    cap += 1;
    for i  in cars:
        val = i.movingForward(turtle,speed)
        if val:
            game_on = False;
            GameOver(score.score)
            break

        if i.xcor() < -320:
            cars.remove(i)
            del i
            print(len(cars))

    #Detect Success crossing
    if turtle.is_at_Finish():
        turtle.gotoStart()
        speed += 10
        score.incrementScore()
    





screen.exitonclick()
