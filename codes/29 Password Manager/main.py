from tkinter import *
from tkinter import messagebox
import random
import pyperclip
# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def generate_password():
    latters = "abcdefghijklmnopqrstuvwxyz"
    number = "1234567890"
    symbol = "!#$%&*()+"
    condition = True
    password = []
    n_letters = random.randint(4,10)
    n_number = random.randint(2, 4)
    n_symbol = random.randint(2, 4)
    #in below random.chioce also help please do check once
    while condition: 
        if(n_letters > 0):
            password.append(latters[random.randint(0,len(latters)-1)])
            n_letters -= 1
        elif(n_number > 0):
            password.append(number[random.randint(0,len(number)-1)])
            n_number -= 1
        elif(n_symbol > 0):
            password.append(symbol[random.randint(0,len(symbol)-1)])
            n_symbol -= 1
            
        if(n_letters == 0 and n_number == 0 and n_symbol == 0):
            condition = False;
    random.shuffle(password)
    print("Password is:","".join(password) )
    password_input.delete(0,END)
    password = "".join(password[:8])
    password_input.insert(0,password)
    pyperclip.copy(password)
# ---------------------------- SAVE PASSWORD ------------------------------- #
def save_data():
    email = email_input.get()
    website = web_input.get()
    password = password_input.get()
    try:
        verify_inputs(email,website,password)
        is_ok = messagebox.askokcancel(title=website, message=f"These are details entered\nEmail: {email}\nPassword: {password}\nis it OK to save?")
        if not is_ok:
            return
        line = f"{website} | {email} | {password}\n"
        with open("password file.txt",mode="a") as f:
            f.write(line)
        clear_all_fields()
    except ValueError as e:
        print(e)
        info_label.config(text=e, fg="red")
        messagebox.showwarning(title="Warning", message=e)

def verify_inputs(email, website, password):
    if len(email) == 0 or len(website) == 0 or len(password) == 0:
        raise ValueError("Please do not leave any fields empty!")

def clear_all_fields():
    email_input.delete(0,END)
    web_input.delete(0,END)
    password_input.delete(0,END)
    info_label["text"] = ""
# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Password Manager")
window.config(padx=50, pady=50)
canvas = Canvas(width=200,height=200, highlightthickness=0)
lock_img = PhotoImage(file="logo.png")
canvas.create_image(101, 101, image=lock_img)
canvas.grid(column=1, row=0)


website = Label(text="Website: ",padx=20)
website.grid(column=0, row=1)

web_input = Entry(width=35)
web_input.grid(column=1, row=1, columnspan=2)
web_input.focus()

################
email_user = Label(text="Email/Username: ", padx=20)
email_user.grid(column=0, row=2)

email_input = Entry(width=35)
email_input.grid(column=1, row=2, columnspan=2)
###############
password = Label(text="Password: ")
password.grid(column=0, row=3)

password_input = Entry(width=21)
password_input.grid(column=1, row=3)


pass_button = Button(text = "Generate Password", 
                command=generate_password, width=14)
pass_button.grid(column=2, row=3)

############################
add_button = Button(text = "Add",  width=36, command=save_data)
add_button.grid(column=1, row=4, columnspan=2)


info_label = Label(text="")
info_label.grid(column=1, row=5)
window.mainloop()
