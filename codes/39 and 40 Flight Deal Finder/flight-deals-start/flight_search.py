import datetime as dt
class FlightSearch:
    #This class is responsible for talking to the Flight Search API.
    def __init__(self, requests, api, token):
        self.requests = requests
        self.API = api
        self.token = token


    def getIATAName(self,city,endpoint):
        URL = self.API + endpoint
        parameter = {
            "term": city,
            "location_types": "city",
            "limit" : 5,
            "active_only": True,
        }
        header = {
            "apikey": self.token,
            "accept": "application/json",
        }
        data = self.requests.get(URL, params=parameter, headers=header)
        return data.json()["locations"][0]["code"]
        
    def search_flight(self,endpoint,from_location,to_location):
        URL = self.API + endpoint
        header = {
            "apikey": self.token,
            "accept": "application/json",
        }
        current = dt.datetime.now()
        now = current.date().strftime("%d/%m/%Y")
        to_date = (current + dt.timedelta(days=20*1)).strftime("%d/%m/%Y")
        
        parameter = {
            "fly_from": from_location,
            "fly_to": to_location,
            "date_from" : now,
            "date_to": to_date
        }
        
        data = self.requests.get(URL, params=parameter, headers=header)
        return self.filter_direct_flights(data.json())

    def filter_direct_flights(self, data):
        flights = data["data"]
        price = 0
        for flight in flights:
            if len(flight['route']) == 1 and (price == 0 or price > flight['price']):
                price = flight["price"]
               
        return price