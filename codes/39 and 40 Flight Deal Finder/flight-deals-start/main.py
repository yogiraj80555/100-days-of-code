import requests
import datetime as dt
from data_manager import DataManager
from flight_search import FlightSearch


TEQUILA_KIWI = "aVk_fArrGgDjTo8qw3u0EMMw78gp4R80"
KIWI_API = "https://api.tequila.kiwi.com/"
SHEET = "https://api.sheety.co/eccec59d4f4e7ca2eb22e4e95b10b5d9/flightDeals/"
# Sheet Link: https://docs.google.com/spreadsheets/d/1sQpV-XuPRI7y2Qc3L__2681Mzs8QEvOe4roGLwBHYqg/edit?usp=sharing
CURRENT_CITY = "LON"

sheetManager = DataManager(requests,SHEET)
flightSerach = FlightSearch(requests,KIWI_API,TEQUILA_KIWI)

#Get Sheet Data
sheet_records = sheetManager.getSheetdata(sheetname="prices")

#puttng recored into excel if IATA not found
for count,record in enumerate(sheet_records):
    if len(record["iataCode"]) < 1:
       iata_code = flightSerach.getIATAName(record["city"], "locations/query/") 
       record["iataCode"] = iata_code
       sheetManager.putRecord(sheetname="prices",record=record)

#asked user Details to add into user sheet
print("Welcome to Yogiraj Flight Club\nwe find the best flight details and mail you")
f_name = input("Enter your first name: ")
l_name = input("Enter your last name: ")
email = input("Enter your email: ")
record = {
    "firstName" : f_name,
    "lastName" : l_name,
    "email" : email
    }
sheetManager.appendRecord(sheetname="users",record=record)

# Updated sheet data with IATA code 
sheet_records = sheetManager.getSheetdata(sheetname="prices")
for count,record in enumerate(sheet_records):
    price = flightSerach.search_flight("search",CURRENT_CITY,record["iataCode"])
    
    #send SMS using twilo
    print(record["iataCode"],": ", price, "SMS sent")

print("Finished!")