class DataManager:
    # #This class is responsible for talking to the Google Sheet.
    # pass

    def __init__(self, request, api) :
        self.requests = request
        self.api = api

    def getSheetdata(self,sheetname):
        api = self.api + sheetname
        data = self.requests.get(api).json()
        if len(data) > 0:
            return data[sheetname]

    def putRecord(self, sheetname, record):
        api = self.api + sheetname
        URL = api + "/" + str(record["id"])
        parameter = {
            sheetname[:-1]: record,
        }
        self.requests.put(URL, json = parameter )
    
    def appendRecord(self, sheetname, record):
        api = self.api + sheetname
        print(api)
        parameter = {
            sheetname[:-1]: record,
        }
        data = self.requests.post(api, json = parameter )
        print(data)