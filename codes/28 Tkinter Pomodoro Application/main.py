from tkinter import *
from urllib import response
# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 1
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
CHECK_MARK = "✔"
reps = 0
timer = None

def reset():
    window.after_cancel(timer)
    canvas.itemconfig(timer_txt, text="00.00")
    chk_mark.config(text="")
    title_label.config(text="Timer",fg=GREEN)


def start_timer():
    global reps
    reps += 1
    work = WORK_MIN * 60
    short_break = SHORT_BREAK_MIN * 60
    long_break = LONG_BREAK_MIN * 60

    if reps % 8 == 0:
        count_down(long_break,"Long BreaK",RED)
    elif reps % 2 == 0:
        count_down(short_break,"Short Break",PINK)
    else:
        count_down(work,"Work",GREEN)



    

def count_down(count,data,color):
    count_min = count//60
    count_sec = count%60
    time_txt = "{:02d}:{:02d}".format(count_min,count_sec)
    canvas.itemconfig(timer_txt, text=time_txt)
    title_label.config(text=data,fg=color)
    
    if count > 0:
        global timer
        timer = window.after(1000,count_down,count-1,data,color)
    else:
        global reps
        print(reps)
        if reps % 2 == 1:
            chk_mark.config(text=CHECK_MARK)
        start_timer()


window = Tk()
window.title("Pomodoro")
window.config(padx=100, pady=40, bg=YELLOW)





title_label = Label(text="Timer", fg=PINK, bg=YELLOW, font=("Arial",28,"normal"))
title_label.grid(column=1,row=0)

canvas = Canvas(width=200,height=224, bg=YELLOW, highlightthickness=0)
tomoto_img = PhotoImage(file="tomato.png")
canvas.create_image(101, 112, image=tomoto_img)
timer_txt = canvas.create_text(101,130, text="00.00", 
                    fill="white", 
                    font=("Arial",28,"bold"))
canvas.grid(column=1, row=2)

start_btn = Button(text="Start", highlightthickness=0,command=start_timer)
start_btn.grid(column=0,row=3)

reset_btn = Button(text="Reset", 
    highlightthickness=0,
    command=reset)
reset_btn.grid(column=2,row=3)

chk_mark = Label(
            highlightthickness=0, bg=YELLOW, fg=GREEN)
chk_mark.grid(column=1, row=3)


window.mainloop()