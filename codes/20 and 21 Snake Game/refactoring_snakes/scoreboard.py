from turtle import Turtle

TEXT_ALIGN = "center"
FONT = ("courier",10,"italic bold")
class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(x=0, y=280)
        self.writeScore()
        

    def getScroreBoardText(self):
        return "Score: "+str(self.score);

    def incrementScore(self):
        self.clearScore()
        self.score += 1
        self.writeScore()

    def clearScore(self):
        self.clear()

    def writeScore(self):
        self.write(self.getScroreBoardText(),
            align=TEXT_ALIGN,
            font=FONT)

    def getScore(self):
        return self.score