from turtle import  Turtle

MOVE_DISTANCE = 10
#directions
RIGHT = 0
UP = 90
LEFT = 180
DOWN = 270



class Snake:

    def __init__(self):
        self.segments = []
        self.createSnake()
        self.head = self.segments[0]
    
    def createSnake(self):
        square = -22
        for i in range(3):
            self.createSegment(i*square,0)
            
            
        
    def createSegment(self,x_cor,y_cor):
        seg = Turtle(shape="square")
        seg.color("White")
        seg.penup()
        seg.goto(x=x_cor,y=y_cor)
        seg.shapesize(stretch_len=0.5, stretch_wid=0.5)
        self.segments.append(seg)       

    
    def addSegment(self):
        seg = self.segments[-1]
        x_cor = seg.xcor()
        y_cor = seg.ycor()
        self.createSegment(x_cor,y_cor)
            

    def move(self):
        for i in range(len(self.segments) - 1,0,-1):
            x_cor = self.segments[i-1].xcor()
            y_cor = self.segments[i-1].ycor()
            self.segments[i].goto(x_cor,y_cor)
        self.segments[0].forward(MOVE_DISTANCE)
    
    def up(self):
        if (int(self.segments[0].heading()) == DOWN):
            return;
        self.segments[0].setheading(UP);

    def down(self):
        if (int(self.segments[0].heading()) == UP):
            return;
        self.segments[0].setheading(DOWN);

    def left(self):
        if (int(self.segments[0].heading()) == RIGHT):
            return;
        self.segments[0].setheading(LEFT);

    def right(self):
        if (int(self.segments[0].heading()) == LEFT):
            return;
        self.segments[0].setheading(RIGHT);