from turtle import Turtle
import random


class Food(Turtle):

    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.penup()
        self.shapesize(stretch_len=0.5, stretch_wid=0.5)
        self.color("blue")
        self.speed("fastest")
        self.refresh()

    
    def refresh(self):
        #Screen size is 600*600 it means -300 to 300 for x and y
        x = random.randint(-290,290);
        y = random.randint(-290,290);
        self.goto((x,y))
       
