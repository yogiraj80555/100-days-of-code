class Animal:
    def __init__(self):
        self.num_eyes = 2

    def breathe(self):
        print("Inhale, exhale.")  

class Fish(Animal):

    # This __init__ function is auto-generated during python execuation
    # so no required to write it but it's a way of good and safe code style.
    def __init__(self):
        # Will cal init function of super class
        super().__init__()

    def swim(self):
        print("Moving in water.")
        
    def eyes(self):
        print(f"Have {self.num_eyes} eyes")
    
    def breathe(self):
        # know as overriding and calling super class functionality
        # in sub class with some extra addition. 
        super().breathe()
        print("doing this underwater.")

nemo = Fish()
nemo.swim()
nemo.breathe()
nemo.eyes()
