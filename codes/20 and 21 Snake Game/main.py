from turtle import Screen
import time
from refactoring_snakes.Snake import Snake
from refactoring_snakes.food import Food
from refactoring_snakes.scoreboard import ScoreBoard
from refactoring_snakes.gameOver import GameOver


def gameOverSituation(score):
    GameOver(score=score)
    return False


screen = Screen()
screen.setup(width=600, height=600);
screen.bgcolor("black")
screen.title("Welcome to snake Game")
screen.tracer(0)

#
snake = Snake()
food = Food()
score = ScoreBoard()

screen.listen()
screen.onkey(snake.up, "Up")
screen.onkey(snake.down, "Down")
screen.onkey(snake.left, "Left")
screen.onkey(snake.right, "Right")
num = 0
is_start = True



while is_start:
    screen.update()
    time.sleep(0.1)
    snake.move()

    #detect collision with food
    if snake.head.distance(food) < 10:
        food.refresh()
        snake.addSegment()
        score.incrementScore()
    
    #detect collision with wall
    x_cor = snake.head.xcor()
    y_cor = snake.head.ycor()
    if  (x_cor < -299 or x_cor > 298) or (y_cor < -299 or y_cor > 299):
        is_start = gameOverSituation(score.getScore());
        

    # detect Collision with tail.
    # note to reversiong list use list[::-1] it will start from last and end at begining
    for snake_seg in snake.segments[1:]:
        # we could not take snake.segments[0] because 
        # snake.head.distance(nake.segments[0]) < 5 is always less then 5
        if snake.head.distance(snake_seg) < 5:
            is_start = gameOverSituation(score.getScore());





    
    



screen.exitonclick()