from tkinter import *
from tkinter import messagebox
import random
import pyperclip
import json
# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def generate_password():
    latters = "abcdefghijklmnopqrstuvwxyz"
    number = "1234567890"
    symbol = "!#$%&*()+"
    condition = True
    password = []
    n_letters = random.randint(4,10)
    n_number = random.randint(2, 4)
    n_symbol = random.randint(2, 4)
    #in below random.chioce also help please do check once
    while condition: 
        if(n_letters > 0):
            password.append(latters[random.randint(0,len(latters)-1)])
            n_letters -= 1
        elif(n_number > 0):
            password.append(number[random.randint(0,len(number)-1)])
            n_number -= 1
        elif(n_symbol > 0):
            password.append(symbol[random.randint(0,len(symbol)-1)])
            n_symbol -= 1
            
        if(n_letters == 0 and n_number == 0 and n_symbol == 0):
            condition = False;
    random.shuffle(password)
    print("Password is:","".join(password) )
    password_input.delete(0,END)
    password = "".join(password[:8])
    password_input.insert(0,password)
    #pyperclip.copy(password)
# ---------------------------- SAVE PASSWORD ------------------------------- #


def save_data():
    email = email_input.get()
    website = web_input.get()
    password = password_input.get()
    try:
        verify_inputs(email,website,password)
        is_ok = messagebox.askokcancel(title=website, message=f"These are details entered\nEmail: {email}\nPassword: {password}\nis it OK to save?")
        if not is_ok:
            return
        line = f"{website} | {email} | {password}\n"
        dct = { website:{"email":email,"password":password},}
        data = {}
        try:
            with open("password file.json",mode="r") as f:
                #reading the JSON
                data = json.load(f)
        except FileNotFoundError as file_error:
            print(file_error)
            with open("password file.json",mode="w") as f:
                #write JSON into json file
                json.dump(dct,f, indent=2)
        else:
            #update the JSON
            data.update(dct)
            print(data)
            with open("password file.json",mode="w") as f:
                #write JSON into json file
                json.dump(data,f, indent=2)
        finally:
            clear_all_fields()
    except ValueError as e:
        print(e)
        info_label.config(text=e, fg="red")
        messagebox.showwarning(title="Warning", message=e)



def verify_inputs(email, website, password):
    if len(email) == 0 or len(website) == 0 or len(password) == 0:
        raise ValueError("Please do not leave any fields empty!")



def clear_all_fields():
    email_input.delete(0,END)
    web_input.delete(0,END)
    password_input.delete(0,END)
    info_label["text"] = ""

def search_data():
    try:
        with open("password file.json",mode="r") as f:
            data = json.load(f)
    except FileNotFoundError as e:
        print(e)
        messagebox.showwarning(title="Warning", message=e)
    else:
        key = web_input.get()
        try :
            entry = data[key]
        except KeyError as key_e:
            print(key_e, "<- This is error")
            messagebox.showwarning(title="Warning", message=f"{key_e} Not found")
        else:
            
            password = entry["password"]
            email = entry["email"]
            msg = f"Email: {email}\nPassword: {password}"
            is_ok = messagebox.askquestion(title=key, message=msg)
            if is_ok:
                email_input.delete(0,END)
                password_input.delete(0,END)
                email_input.insert(0,email)
                password_input.insert(0,password)


# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Password Manager")
window.config(padx=50, pady=50)
canvas = Canvas(width=200,height=200, highlightthickness=0)
lock_img = PhotoImage(file="logo.png")
canvas.create_image(101, 101, image=lock_img)
canvas.grid(column=1, row=0)


website = Label(text="Website: ",padx=20)
website.grid(column=0, row=1)

web_input = Entry(width=24)
web_input.grid(column=1, row=1)
web_input.focus()


search_button = Button(text = "Search",  width=15, command=search_data)
search_button.grid(column=2, row=1)

################
email_user = Label(text="Email/Username: ", padx=20)
email_user.grid(column=0, row=2)

email_input = Entry(width=43)
email_input.grid(column=1, row=2, columnspan=2)
email_input.insert(0,"yogiraj@email.com")
###############
password = Label(text="Password: ")
password.grid(column=0, row=3)

password_input = Entry(width=24)
password_input.grid(column=1, row=3)


pass_button = Button(text = "Generate Password", 
                command=generate_password, width=15)
pass_button.grid(column=2, row=3)

############################
add_button = Button(text = "Add",  width=40, command=save_data)
add_button.grid(column=1, row=4, columnspan=2)


info_label = Label(text="")
info_label.grid(column=1, row=5)
window.mainloop()
