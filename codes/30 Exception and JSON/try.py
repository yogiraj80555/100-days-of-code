
try:
    file = open("a_file.txt")
    a_dict = {"key":"value"}
    print(a_dict["adad"])
except FileNotFoundError:
    print("file not found")
    with open("a_file.txt","a") as f:
        pass
except KeyError as msg:
    print("KeyError",msg)
else:
    #if try successfully executed then only else block will get executed.
    print("Try Executed Successfully") 
    content = file.read()
    print(content)
finally:
    file.close()
    print("I will get executed at any how no matters what happned.")
    #raising your own error
    raise TypeError("This is an error that I made up.")
