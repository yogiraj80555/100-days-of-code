import pandas

data = pandas.read_csv("nato_phonetic_alphabet.csv")

dct = {row.letter:row.code for (index,row) in data.iterrows()}
while True:
    name = input("Enter a word: ");
    try:
        result = [dct[i.upper()] for i in name ]   
    except KeyError:
        print("Sorry, Only letters in the alphabet please.")
    else:
        print(result)
        break;
    
    


