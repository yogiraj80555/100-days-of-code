def read(path):
    with open(path) as name_file:
        return name_file.read()

def write(path,data):
    with open(path, mode="w") as write_file:
        write_file.write(data)


names = read("./Input/Names/invited_names.txt").split()
data = read("./Input/Letters/starting_letter.txt")
save_file_path = "./Output/ReadyToSend/letter_for_{}.txt"
for i in names:
    content = data.replace("[name]",i).replace("Angela","Yogiraj")
    write(save_file_path.format(i),content)

print(data)
