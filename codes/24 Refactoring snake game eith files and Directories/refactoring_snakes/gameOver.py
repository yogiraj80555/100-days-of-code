from turtle import Turtle

TEXT_ALIGN = "center"
FONT = ("Times New Roman",15,"italic bold")
class GameOver(Turtle):

    def __init__(self,score):
        super().__init__()
        self.color("white")
        self.penup()
        self.hideturtle()
        self.writeScore(self.gameOverText(score))

    def gameOverText(self,score):
        return f"GAME OVER!\nYour Score is: {score}";

    def writeScore(self,data):
        self.write(data,
            align=TEXT_ALIGN,
            font=FONT)