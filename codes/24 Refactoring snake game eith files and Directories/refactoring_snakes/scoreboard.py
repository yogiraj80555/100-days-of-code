from turtle import Turtle

TEXT_ALIGN = "center"
FONT = ("courier",10,"italic bold")
class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.high_score = self.readHighScore()
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(x=0, y=280)
        self.writeScore()
        

    def getScroreBoardText(self):
        return f"Score: {self.score}  High Score: {self.high_score}";

    def incrementScore(self):
        self.clearScore()
        self.score += 1
        self.writeScore()

    def clearScore(self):
        self.clear()

    def writeScore(self):
        self.write(self.getScroreBoardText(),
            align=TEXT_ALIGN,
            font=FONT)

    def getScore(self):
        return self.score

    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
        self.score = 0
        self.writeHighScore()
        self.clearScore()
        self.writeScore()

    def readHighScore(self):
        with open("high_score.txt", mode="r") as file:
            data = file.read()
            if len(data) == 0:
                return 0
            return int(data)
    
    def writeHighScore(self):
        with open("high_score.txt", mode="w") as file:
            file.write(str(self.high_score))
        
        