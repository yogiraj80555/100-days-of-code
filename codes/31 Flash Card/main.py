from importlib.metadata import PathDistribution
from tkinter import *
import pandas
import random
import time

GREEN = "#B1DDC6"
try:
    data = pandas.read_csv("words_to_learn.csv")
except FileNotFoundError:
    data = pandas.read_csv("words.csv")

dct = data.to_dict(orient="records")
lst = [[data.iloc[i]["French"],data.iloc[i]["English"]] for i in range(len(data))]
time_fliping = 1
word = None

#------------------------------ ------------------------------------------------------#
def right_guess():
    dct.remove(word)
    df = pandas.DataFrame(dct)
    df.to_csv("words_to_learn.csv",index=False)
    next_card()

def wrong_guess():
    next_card()

def next_card():
    global time_fliping,word
    window.after_cancel(time_fliping)
    word = random.choice(dct)
    print_card("French", word["French"], "black", front_img)
    time_fliping = window.after(4000,print_card,"English",word["English"],
        "white", back_img)

def print_card(language_text,word,fill,image):
    canvas.itemconfig(canvas_image, image=image)
    canvas.itemconfig(language_txt, text=language_text, fill=fill)
    canvas.itemconfig(word_txt, text=word, fill=fill)
#------------------------------ ------------------------------------------------------#
    
#------------------------------ ------------------------------------------------------#
window = Tk()
window.title("Flash Card")
window.config(padx=50,pady=50,bg=GREEN)
canvas = Canvas(width=800,height=526, highlightthickness=0)
back_img = PhotoImage(file="images/card_back.png")
front_img = PhotoImage(file="images/card_front.png")
canvas_image = canvas.create_image(400, 263, image=front_img)
canvas.config(bg=GREEN)
language_txt = canvas.create_text(400,150, text="Title", 
                    fill="black", 
                    font=("Arial",40,"italic"))
word_txt = canvas.create_text(400,263, text="Word", 
                    fill="black", 
                    font=("Arial",60,"bold"))
canvas.grid(column=0, row=0, columnspan=2)

cross_image = PhotoImage(file="images/wrong.png")
wrong_button = Button(image=cross_image,highlightthickness=0,command=wrong_guess)
wrong_button.grid(column=0,row=1)

right_image = PhotoImage(file="images/right.png")
right_button = Button(image=right_image,highlightthickness=0,command=right_guess)
right_button.grid(column=1,row=1)

next_card()
window.mainloop()