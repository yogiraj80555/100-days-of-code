from tkinter import *
from typing import Tuple
from xmlrpc.client import boolean
from question.quiz_brain import QuizBrain
import time
THEME_COLOR = "#375362"

class QuizInterface:

    def __init__(self, quiz_Brain: QuizBrain): #required quiz brain object only
        self.window = Tk()
        self.window.title("Quizller App")
        self.window.config(bg=THEME_COLOR, padx=20, pady=20)
        self.score_label = Label(text="Score: 0", fg="white", bg=THEME_COLOR)
        self.score_label.grid(row=0, column=1)
        self.canvas = Canvas(width=300,height=250, highlightthickness=0, bg="white")
        canvas_image = self.canvas.create_image(150, 125)
        self.question_txt = self.canvas.create_text(
            150,
            100,
            text="Title", 
            fill="black", 
            font=("Arial",16,"italic"),
            width=280)
        self.canvas.grid(column=0, row=1, columnspan=2,pady=20)
        
        right_image = PhotoImage(file="images/true.png")
        self.right_button = Button(image=right_image,highlightthickness=0, command=self.trueButton)
        self.right_button.grid(column=0,row=2)

        cross_image = PhotoImage(file="images/false.png")
        self.wrong_button = Button(image=cross_image,highlightthickness=0, command=self.falseButton)
        self.wrong_button.grid(column=1,row=2)
        self.quiz = quiz_Brain
        self.currentData : Tuple

        
        self.displayNextQuestion()
        self.window.mainloop()

    def displayNextQuestion(self):
        self.canvas.config(bg="white")
        try:
            self.currentData = self.quiz.next_question()
            self.canvas.itemconfig(self.question_txt, text=self.currentData[0])
        except IndexError:
            self.currentData = None
            self.canvas.itemconfig(self.question_txt, 
            text=f"Questions Finish\nFinal Socre is: {self.quiz.getScore()}")
            self.wrong_button.config(state="disabled")
            self.right_button.config(state="disabled")
        


    def falseButton(self):
        self.check_answer(False)

    def trueButton(self):
        self.check_answer(True)

    
    def check_answer(self,guess):
        if self.currentData == None:
            return
        answer:bool
        answer = eval(self.currentData[1]);
        if answer == guess:
            self.quiz.scoreIncrement()
            self.score_label.config(text=f"Score: {self.quiz.getScore()}")
            self.giveFeedback(True)
        else: self.giveFeedback(False)
        
        self.window.mainloop()

    def giveFeedback(self,feedback:bool):
        #time.sleep(2)
        if feedback:
            self.canvas.config(bg="green")
        else: 
            self.canvas.config(bg="red")
        self.window.after(1000,self.displayNextQuestion)

        
   
    