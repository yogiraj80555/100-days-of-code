import html
from typing import Tuple

class QuizBrain:

	def __init__(self,questions):
		self.score: int #setting data type as Integer know as Type Hint
		self.question_number: int #setting data type as Integer
		self.question_number = 0
		self.score = 0
		self.question_list = questions

	def next_question(self) -> Tuple: #this function expected to return Tuple data type
		question = html.unescape(self.question_list[self.question_number].text)
		answer = self.question_list[self.question_number].answer
		self.question_number += 1
		print(question)
		return (f"Q.{self.question_number}: {question}?",answer)

	def still_has_question(self):
		return self.question_number < len(self.question_list)
			
	def getScore(self):
		return self.score;

	def scoreIncrement(self):
		self.score += 1