from question.data import question_data as questions
from question.question_model import Question
from question.quiz_brain import QuizBrain
from ui import QuizInterface
import question.data as q


question_bank = [Question(i["text"],i["answer"]) for i in questions()]
quizBrain = QuizBrain(question_bank)
quizUi = QuizInterface(quizBrain)



 