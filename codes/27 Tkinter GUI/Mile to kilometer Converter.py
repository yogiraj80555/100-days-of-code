from tkinter import *


window = Tk()
window.title("GUI Program")
window.minsize(width=300, height=180)
window.config(padx=10,pady=10)



# Entry
input = Entry()
input.place(x=100,y=10)
input["width"] = 12
print(input.get())

#Label
Label(text = "Miles",
    font=("Arial",12)).place(x=210,y=10)

#Label
Label(text = "is equal to",
    font=("Arial",12)).place(x=20,y=50)

#Label
Label(text = "KM",
    font=("Arial",12)).place(x=210,y=50)

#Label
label = Label(text = "0",
    font=("Arial",12))
label.place(x=110,y=50)

# Button
def button_clicked():
    label.config(text = f"{float(input.get())*1.60934}")
button = Button(text = "Calculate", command=button_clicked)
button.place(x=80,y=100)



window.mainloop()