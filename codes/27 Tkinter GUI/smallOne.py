from tkinter import *

window = Tk()
window.title("GUI Program")
window.minsize(width=300, height=200)
window.config(padx=20,pady=20)

#Label
label = Label(text = "This is label",
    font=("Arial",24,"bold"))
label.grid(column=0,row=0)
label.config(padx=20,pady=20)


# Button
def button_clicked():
    print("I got Clicked")
    label.config(text = "Button got Clicked")
    input.get()
    label.config(text = input.get())
button = Button(text = "Click me", command=button_clicked)
button.grid(column=1,row=2)

# Button
def button_clicked1():
    print("I got Clicked")
    label.config(text = "Button got Clicked")
    input.get()
    label.config(text = input.get())
button1 = Button(text = "Click me", command=button_clicked1)
button1.grid(column=2,row=0)




# Entry
input = Entry()
input.insert(10,string="Some Text")
input.grid(column=3,row=4)

input["width"] = 10
print(input.get())




window.mainloop()