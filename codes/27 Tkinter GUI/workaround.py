from __future__ import division


def add(*args):
    sum = 0
    for i in args:
       sum += i
    return sum

print(add(1,2,3,4,5,6,7,8,9))


def keywards_args(n,**kwargs):
    print(n,kwargs)
    for key, value in kwargs.items():
        print(key," -> ",value)
    print(n+kwargs["add"])
    print(n*kwargs["multiplay"])
    print(n/kwargs["division"])
    print(kwargs.get("notKey")," Here we used dict.get() which not throw error is key not found, instead it return 'None'. ")

keywards_args(10,add=10,multiplay=30,division=40,sum=90)