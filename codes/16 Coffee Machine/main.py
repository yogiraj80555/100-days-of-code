from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

menu = Menu()
coffee_maker = CoffeeMaker()
money_machine = MoneyMachine()

while True:
	choice = input(f"What would you like to? ({menu.get_items()[:-1]}): ").lower()

	if choice == "report":
		coffee_maker.report()
		money_machine.report()
		continue;

	food = menu.find_drink(choice);
	if food == None:
		continue;
		
	elif coffee_maker.is_resource_sufficient(food) :
		if(money_machine.make_payment(food.cost)):
			coffee_maker.make_coffee(food)
