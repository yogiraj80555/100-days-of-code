class QuizBrain:

	def __init__(self,questions):
		self.question_number = 0
		self.score = 0
		self.question_list = questions

	def next_question(self):
		question = self.question_list[self.question_number].text
		answer = self.question_list[self.question_number].answer
		self.question_number += 1
		ans = input(f"Q.{self.question_number}: {question} (True/False)?: ").lower()

		if ans == answer.lower():
			print("You got Right")
			self.score += 1
		else:
			print("That;s Wrong correct is:",answer)
		print("Your Current Score is:",self.score,"\n")


	def still_has_question(self):
		return self.question_number < len(self.question_list)
			