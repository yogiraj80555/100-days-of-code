import requests
import datetime as dt
URL = "http://api.sunrise-sunset.org/json"


parameters = {
    "lat":18.520430,
    "lng":73.856743,
    "formatted":0,
}

response = requests.get(url=URL, params=parameters)
response.raise_for_status()
data = response.json()
sunrise = data["results"]["sunrise"].split("T")[1].split(":")[0]
sunset = data["results"]["sunset"].split("T")[1].split(":")[0]
print(sunrise, sunset)
print(dt.datetime.now().hour)


"""
{'results': {'sunrise': '12:55:41 AM', 
            'sunset': '12:48:06 PM', 
            'solar_noon': '6:51:53 AM', 
            'day_length': '11:52:25', 
            'civil_twilight_begin': '12:34:55 AM', 
            'civil_twilight_end': '1:08:52 PM', 
            'nautical_twilight_begin': '12:09:33 AM', 
            'nautical_twilight_end': '1:34:14 PM', 
            'astronomical_twilight_begin': '11:44:14 PM', 
            'astronomical_twilight_end': '1:59:33 PM'
            }, 
'status': 'OK'}
"""