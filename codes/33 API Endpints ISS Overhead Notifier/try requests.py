import requests
URL = "http://api.open-notify.org/iss-now.json"
response = requests.get(url=URL)
    # if response.status_code != 200:
    #     raise ResourceWarning(f"Bad Response from ISS API status {response.status_code}")
#Alternate of above
response.raise_for_status()
data = response.json()
longitude = data["iss_position"]["latitude"] 
latitude = data["iss_position"]["longitude"]
print(latitude,longitude)