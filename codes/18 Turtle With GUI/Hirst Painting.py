import turtle as t
import random
color_list = [(33, 3, 238), (61, 3, 60), 
(247, 249, 254), (250, 126, 2), (3, 251, 4), (243, 135, 31), 
(245, 3, 0), (251, 250, 29), (206, 5, 70), (26, 242, 29), 
(55, 7, 252), (254, 254, 0), (151, 56, 85), (102, 75, 227), 
(215, 6, 90), (215, 53, 83), (97, 245, 98), (191, 144, 178), 
(254, 7, 4), (160, 159, 254), (230, 53, 48), (228, 158, 217), 
(201, 40, 33), (254, 157, 152), (95, 48, 195), (158, 121, 245)]
lines_of_dots = 10


t.colormode(255)
tim = t.Turtle()
tim.penup()
tim.hideturtle()
tim.setheading(225)
tim.forward(300)
tim.setheading(0)
# for i in range(lines_of_dots):

#     for _ in range(10):
#         tim.dot(20,random.choice(color_list))
#         tim.forward(50)

#     tim.setheading(90)
#     tim.forward(40)
#     tim.setheading(180)
#     tim.forward(500)
#     tim.setheading(0) #on towards to right

##This code can be
for i in range(1,101):
   
    tim.pendown()
    tim.dot(20,random.choice(color_list))
    tim.penup()
    tim.forward(50)

    if i % 10 == 0:
        tim.setheading(90)
        tim.forward(40)
        tim.setheading(180)
        tim.forward(500)
        tim.setheading(0) #on towards to right

screen = t.Screen()
screen.exitonclick()