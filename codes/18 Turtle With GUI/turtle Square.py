from turtle import Turtle, Screen

obj = Turtle()
obj.shape("turtle")
obj.color("green")
#turtle Straight Square
for _ in range(4):
    obj.forward(200)
    obj.right(90)

obj.forward(250)
obj.color("pink")
#turtle reverse Square
for _ in range(4):
    obj.forward(-200)
    obj.right(90)


screen = Screen()
screen.exitonclick()