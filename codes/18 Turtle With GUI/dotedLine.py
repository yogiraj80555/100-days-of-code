from turtle import Turtle, Screen

obj = Turtle()
obj.shape("turtle")
obj.color("green")

for _ in range(80):
    if _%2 == 0:
        obj.penup()
    else:
        obj.pendown()
    obj.forward(4)


screen = Screen()
screen.exitonclick()