from turtle import Turtle, Screen
import turtle as t
import random
obj = Turtle()
obj.shape("turtle")
obj.color("green")
obj.pensize(3)
obj.speed(0)
t.colormode(255)
steps = random.randint(100,500)
print(steps)
lst = ["navy","green yellow", "teal", "light blue", 
"pale goldenrod", "light salmon", "medium purple", "lavender",
"gold", "light steel blue"]
for steps in range(steps):
    obj.setheading(random.randint(10,350))
    obj.forward(40)
    obj.pencolor((random.randint(1,255),random.randint(1,255),random.randint(1,255)))


screen = Screen()
screen.exitonclick()