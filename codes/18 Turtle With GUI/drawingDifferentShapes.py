from turtle import Turtle, Screen

obj = Turtle()
obj.shape("turtle")
obj.color("green")
colorInt = 215078
for i in range(3,12):
    angle = 360/i 
    while i != 0:
        obj.forward(70)
        obj.right(angle)
        colorInt += 10000
        obj.color(f"#{colorInt}")
        i -= 1
    




screen = Screen()
screen.exitonclick()