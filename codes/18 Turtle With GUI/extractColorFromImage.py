import colorgram

# Extract 6 colors from an image.
colors = colorgram.extract('./18 Turtle With GUI/image.jpg', 30)

# colorgram.extract returns Color objects, which let you access
# RGB, HSL, and what proportion of the image was that color.
first_color = colors[0]
rgb = first_color.rgb # e.g. (255, 151, 210)
hsl = first_color.hsl # e.g. (230, 255, 203)

print(len(colors),colors)
print(rgb)
print(hsl)
rgb_colors = []
for i in colors:
    rgb_colors.append((i.rgb.r, i.rgb.g, i.rgb.b))

print(rgb_colors)


color_list = [(33, 3, 238), (61, 3, 60), 
(247, 249, 254), (250, 126, 2), (3, 251, 4), (243, 135, 31), 
(245, 3, 0), (251, 250, 29), (206, 5, 70), (26, 242, 29), 
(55, 7, 252), (254, 254, 0), (151, 56, 85), (102, 75, 227), 
(215, 6, 90), (215, 53, 83), (97, 245, 98), (191, 144, 178), 
(254, 7, 4), (160, 159, 254), (230, 53, 48), (228, 158, 217), 
(201, 40, 33), (254, 157, 152), (95, 48, 195), (158, 121, 245)]