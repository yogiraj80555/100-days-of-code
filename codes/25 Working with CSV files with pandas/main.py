from os import getcwd
import pandas

print(getcwd())
data = pandas.read_csv("weather_data.csv")
print(data["temp"][:5])
print(data.to_dict())
print(data["temp"].to_list())
print("Average:",data["temp"].mean())
print("Max:",data.temp.max())
print("Min:",data.temp.min())

print("Return specific row: \n", data[data.day == "Monday"])
print("\n\nFilter specific row: \n", data[data.day == "Monday"].condition)
print("\n\nFilter specific row: \n", int(data[data.day == "Monday"].temp)*9/5+32)


