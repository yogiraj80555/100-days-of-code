import turtle
import pandas

screen = turtle.Screen()
screen.title("U.S. States Game")

image = "./us-states-game/blank_states_img.gif";
screen.addshape(image)
turtle.shape(image)

def get_mouse_click_coor(x ,y):
    print(x, y)


def createTurtle(x, y, state):
    t = turtle.Turtle()
    t.hideturtle()
    t.penup()
    t.goto(x,y)
    t.write(state)
    




data = pandas.read_csv("./us-states-game/50_states.csv")
count = len(data)
lst = data.state.to_list()

found = 0
game_on = True
while game_on:
    ans = screen.textinput(title=f"{found}/{count} States Correct", prompt="Enter State Name below").title()
    if ans in lst:
        lst.remove(ans)
        found += 1
        row = data[data.state == ans]
        createTurtle(int(row.x), int(row.y), ans)
    if ans == "Q":
        print(lst)
        df = pandas.DataFrame(lst)
        df.to_csv("missing_States.csv")
        break;




turtle.onscreenclick(get_mouse_click_coor)


#turtle.mainloop()


screen.exitonclick()
