from os import getcwd
import pandas

print(getcwd())
data = pandas.read_csv("Squirrel_Data.csv")
gray_color = len(data[data["Primary Fur Color"] == "Gray"])
red_color = len(data[data["Primary Fur Color"] == "Cinnamon"])
black_color = len(data[data["Primary Fur Color"] == "Black"])
print(set(data["Primary Fur Color"]))
data_dict = {
    "Fur Color":["Gray", "Cinnamon", "Black"],
    "Count": [gray_color, red_color, black_color]
}

df = pandas.DataFrame(data_dict)
df.to_csv("squirrel_count.csv")