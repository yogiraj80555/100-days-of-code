import requests
import datetime
G_SHEET_URL = "https://api.sheety.co/eccec59d4f4e7ca2eb22e4e95b10b5d9/myWorkouts/workouts"
APP_ID = "APP_ID"
APP_SECRET = "APP_SECRET"
EXERCISE_ENDPOINT = "/v2/natural/exercise"
DOMAIN = "https://trackapi.nutritionix.com"
header = {
    "x-app-id": APP_ID,
    "x-app-key": APP_SECRET,
    "x-remote-user-id":"0"
}

json_body = {
    "query" : input("Enter your workout query: "),
}
date = datetime.datetime.now().strftime("%d/%m/%Y,%H:%M:%S").split(",")
url = DOMAIN+EXERCISE_ENDPOINT;
response = requests.post(url, json=json_body, headers=header)
data = response.json()
data = data["exercises"]

for i in data:
    name  = i["name"]
    duration = i["duration_min"]
    calories = i["nf_calories"]
    content = {
        "date": date[0],
        "time": date[1],
        "exercise": name,
        "duration": duration,
        "calories": calories
    }
    post_data = { "workout": content}
    response = requests.post(url=G_SHEET_URL, json=post_data)
    print(name,calories,response.status_code)
#Run 23 miles and walked for 18 km

print(date)